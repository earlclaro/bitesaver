package com.myapp.bitesaver;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StartBudgetActivity extends AppCompatActivity {

    private EditText startDateInput, endDateInput, initialBalanceInput, displayNameInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.startbudget);

        startDateInput = findViewById(R.id.start_date);
        endDateInput = findViewById(R.id.end_date);
        initialBalanceInput = findViewById(R.id.initial_balance);
        displayNameInput = findViewById(R.id.display_name);

        Button nextButton = findViewById(R.id.next_button);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get the user inputs
                String startDate = startDateInput.getText().toString();
                String endDate = endDateInput.getText().toString();
                String initialBalance = initialBalanceInput.getText().toString();
                String displayName = displayNameInput.getText().toString();

                // Do some basic validation to ensure inputs are not empty
                if (startDate.isEmpty() || endDate.isEmpty() || initialBalance.isEmpty() || displayName.isEmpty()) {
                    Toast.makeText(StartBudgetActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
                } else {
                    // TODO: Implement next button functionality here
                }
            }
        });
    }
}
