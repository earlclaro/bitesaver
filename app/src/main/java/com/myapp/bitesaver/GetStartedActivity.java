package com.myapp.bitesaver;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class GetStartedActivity extends AppCompatActivity {

    private EditText mNameEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getstarted);

        mNameEditText = findViewById(R.id.name_edittext);

        Button startBudgetButton = findViewById(R.id.getstarted_button);
        startBudgetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = mNameEditText.getText().toString();

                // Start the StartBudgetActivity and pass the user's name as an extra
                Intent intent = new Intent(GetStartedActivity.this, StartBudgetActivity.class);
                intent.putExtra("name", name);
                startActivity(intent);
            }
        });
    }
}

